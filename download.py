# download.py
# import datetime
# https://medium.com/analytics-vidhya/crypto-price-forecasting-with-bi-gru-c9bcec76b69f
# TODO:
# - read medium.com analytics article
# - create an account on lunarcrush.com
# - download data from a different time period and save it to a different csv file (timeseries function)
# - play around with api.lunarcrush.com

from urllib import request
import json
import numpy as np
import pandas as pd
import sys
from pathlib import Path


from env import API_KEY, DATA_DIR


def fourier_components(series, phase=0, period=None):
    components = pd.DataFrame(index=series.index)
    period = 1 + series.max() if period is None else period
    t = np.pi * (series - phase) / period
    components['sin'] = np.sin(t)
    components['cos'] = np.cos(t)
    return components


def get_date_features(data, columns=['index']):
    dfs = [data]
    for c in columns:
        if c not in data.columns and c == 'index':
            dates = data.index
        else:
            dates = data[c]
        df = pd.DataFrame(index=data.index)

        df[f'{c}_month'] = dates.dt.month
        df[f'{c}_day'] = dates.dt.day
        df[f'{c}_weekday'] = dates.dt.weekday
        df[f'{c}_weekofyear'] = dates.dt.isocalendar().week
        df[f'{c}_quarter'] = df[f'{c}_month'] % 3

        # quarter, month, and day will have redundant sin/cos component values:
        for cyclename in 'quarter month day weekday'.split():
            components = fourier_components(df[f'{c}_{cyclename}'])
            df[f'{c}_{cyclename}_sin'] = components['sin']
            df[f'{c}_{cyclename}_cos'] = components['cos']

        df[f'{c}_year'] = dates.dt.year
        df[f'{c}_isweekend'] = df[f'{c}_weekday'] >= 5  # 0 = Mon, 6 = Sun
        df = pd.get_dummies(df, columns=f'{c}_quarter {c}_month {c}_weekday'.split())
        dfs.append(df)
    return pd.concat(dfs, axis=1)


def timeseries(
        symbols='BTC ETH BAT ALGO'.split(),
        features='time,close,percent_change_24h,tweets,social_score,news'.split(','),
        n=720,  # integer <= 720
        start=None,  # int seconds since 1970, e.g. 1634601600 = pd.Timestamp(2021, 10, 19).timestamp()
        end=None,  # 1634659528 from `int(pd.Timestamp.now().timestamp())` for oct 19, 2021
        interval='day'
):
    end = end or int(pd.Timestamp(pd.Timestamp.now().date()).timestamp())
    symbols = symbols.split(',') if ',' in symbols else symbols
    symbols = symbols.split() if isinstance(symbols, str) else symbols
    # Assets Endpoint request
    dfs = []
    features = ','.join(features)
    symbols_not_found = []
    for symbol in symbols:
        url = f"https://api.lunarcrush.com/v2?" \
              + f"&data=assets" \
              + f"&key={API_KEY}" \
              + f"&symbol={symbol}" \
              + f"&interval={interval}" \
              + f"&time_series_indicators={features}"\
              + f"&data_points={n}" \
              + ("" if not start else f"&start={start}") \
              + ("" if not end else f"&end={end}")
        obj = json.load(request.urlopen(url))
        df = pd.DataFrame(obj['data'][0]['timeSeries'])
        if df is None or not len(df):
            symbols_not_found.append(symbol)
            print(f'Unable to find any data for {symbol} and {pd.Timestamp(end * 1e9)}')
        else:
            if 'time' in df.columns:
                df = df.set_index('time')
            dfisna = df.isna()
            dfisna.columns = [f'{c}_isna' for c in df.columns]
            df = pd.concat([df, dfisna], axis=1)
            df = df.fillna(df.median())
            df.columns = [f'{symbol}_{c}' for c in df.columns]
            dfs.append(df)

    if len(dfs):
        df = pd.concat(dfs, axis=1)
    else:
        return dict(df=pd.DataFrame(), symbols_not_found=symbols_not_found, end=end)
    del dfs
    df['datetime'] = pd.to_datetime(list(df.index.values), utc=True, unit='s')
    df = get_date_features(df, columns=['datetime'])
    del df['datetime']
    df = df.reset_index()

    # df['time'] = df['time'].apply(lambda x: datetime.utcfromtimestamp(x))
    return dict(df=df, symbols_not_found=symbols_not_found, end=end, symbols=symbols)


def windowingData(start, end, window_size, dataset):
    data, labels = [], []
    idx = start
    while (idx + window_size) < end:
        # adding sequence of length window_size
        data.append(dataset[idx: idx + window_size])
        # adding the close price of next point
        labels.append(dataset[idx + window_size, 0])
        idx += 1
    return np.array(data), np.array(labels)


def train_test_split(df, window_size=14, num_test=None):
    # fetching train windowed data
    window_size = window_size or 14
    num_test = num_test or window_size * 2
    num_train = int(len(df) - num_test)
    X_train, y_train = windowingData(
        start=0,
        end=num_train,
        window_size=window_size,
        dataset=df.values
    )

    # fetching test windowed data
    X_test, y_test = windowingData(
        start=num_train,
        end=len(df),
        window_size=window_size,
        dataset=df.values
    )
    return dict(X_train=X_train, y_train=y_train, X_test=X_test, y_test=y_test)


def main(symbols=['BTC', 'ALGO', 'ETH', 'BAT'], end=None, n=720):
    now = pd.Timestamp.now()

    if not end:
        end = int(now.timestamp())
        if len(sys.argv) > 1:
            end = int(pd.Timestamp(*[int(x) for x in sys.argv[1].split('-')]).timestamp())
    if not symbols:
        symbols = ['BTC', 'ALGO', 'ETH', 'BAT']
        if len(sys.argv) > 2:
            symbols = [s.strip() for s in symbols.split(',')]
    print(f'Downloading {n} datapoints for {symbols} ending on {pd.Timestamp(end * 1e9)}')
    results = timeseries(
        n=n,
        end=end,
        symbols=symbols)

    df = results['df']
    symbols_not_found = results['symbols_not_found']
    filename = f"{'-'.join([s for s in symbols if s not in symbols_not_found])}" \
        + f"+end={end.year}-{end.month}-{end.day}" \
        + f"+downloaded={now.year}-{now.month}-{now.day}" \
        + f"-{now.hour}-{now.minute}-{now.second}.csv"

    df.to_csv(Path(DATA_DIR) / filename)
    print(f'Saved {len(df)} datapoints to {DATA_DIR / filename}')
    return df


if __name__ == '__main__':
    main()
