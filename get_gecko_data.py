import time

from pycoingecko import CoinGeckoAPI
import pandas as pd
import yaml


if __name__ == '__main__':
    """
        cg = CoinGeckoAPI()
        x = cg.get_price(
            ids='bitcoin',
            vs_currencies='usd',
            include_market_cap='true',
            include_24hr_vol='true',
            include_24hr_change='true',
            include_last_updated_at='true')
    """
    coin_dfs = {}
    end_date = '2022-04-26'
    coin_name = 'the-graph'
    for p in range(9):
        coin_dfs[coin_name] = []
        url = f'https://www.coingecko.com/en/coins/{coin_name}/historical_data?end_date={end_date}&start_date={start_date}&page={p+1}'
        resp = requests.get(url)
        print(resp)
        df = pd.read_html(resp.text)
        if df and len(df[0]):
            dfs.extend(df)
        else:
            break
    df = pd.concat(dfs)


    cg = CoinGeckoAPI()
    dates = pd.date_range(start='2018-01-01', end='2021-01-01', freq='D')
    coins = cg.get_coins()
    all_coinids = [c['id'] for c in coins]
    # algorand not available in 2018
    coinids = [i for i in all_coinids if i in 'bitcoin bitcoin-cash ethereum tether ripple'.split()]

    datestrs = dates.strftime(r'%d-%m-%Y')
    filename = f'coingecko-prices-{datestrs[0]}-{datestrs[-1]}.yaml'

    data = {}
    with open(filename, 'w') as fout:
        for d, dt in zip(datestrs, dates):
            print(d)
            row = {}
            for coinid in coinids:
                print(coinid)
                resp = None
                for delay in [6, 60, 360]:
                    time.sleep(delay)
                    try:
                        resp = cg.get_coin_history_by_id(coinid, d)
                        break
                    except Exception as e:
                        print(e)
                if resp:
                    market_data = resp.get('market_data', {})
                    prices = market_data.get('current_price', {})
                    volumes = market_data.get('total_volume', {})
                    row.update({
                        coinid + '_price_' + k: v for k, v
                        in prices.items()
                        })
                    row.update({
                        coinid + '_vol_' + k: v for k, v
                        in volumes.items()
                        })
                    print(f'Retrieved {len(prices)} prices and {len(volumes)} volumes for {coinid} ({len(row)} total).')
            row['gecko_datestr'] = d
            row['datetime'] = dt
            data[d] = row
            text = yaml.dump([row])
            fout.writelines([text])
    df = pd.DataFrame(data).T
    df.index = dates
    df.to_csv(filename + '.csv')
    print(df.head())
    print(df.tail())


"""
 /simple/price endpoint with the required parameters
>>> cg.get_price(ids='bitcoin', vs_currencies='usd')
{'bitcoin': {'usd': 3462.04}}

>>> cg.get_price(ids='bitcoin,litecoin,ethereum', vs_currencies='usd')
# OR (lists can be used for multiple-valued arguments)
>>> cg.get_price(ids=['bitcoin', 'litecoin', 'ethereum'], vs_currencies='usd')
{'bitcoin': {'usd': 3461.27}, 'ethereum': {'usd': 106.92}, 'litecoin': {'usd': 32.72}}

>>> cg.get_price(ids='bitcoin,litecoin,ethereum', vs_currencies='usd,eur')
# OR (lists can be used for multiple-valued arguments)
>>> cg.get_price(ids=['bitcoin', 'litecoin', 'ethereum'], vs_currencies=['usd', 'eur'])
{'bitcoin': {'usd': 3459.39, 'eur': 3019.33}, 'ethereum': {'usd': 106.91, 'eur': 93.31}, 'litecoin': {'usd': 32.72, 'eur': 28.56}}

# optional parameters can be passed as defined in the API doc (https://www.coingecko.com/api/docs/v3)
>>> cg.get_price(ids='bitcoin', vs_currencies='usd', include_market_cap='true',
... include_24hr_vol='true', include_24hr_change='true', include_last_updated_at='true')
{'bitcoin': {'usd': 3458.74, 'usd_market_cap': 60574330199.29028, 'usd_24h_vol': 4182664683.6247883,
... 'usd_24h_change': 1.2295378479069035, 'last_updated_at': 1549071865}}
# OR (also booleans can be used for boolean type arguments)
>>> cg.get_price(ids='bitcoin', vs_currencies='usd', include_market_cap=True, include_24hr_vol=True,
... include_24hr_change=True, include_last_updated_at=True)
{'bitcoin': {'usd': 3458.74, 'usd_market_cap': 60574330199.29028, 'usd_24h_vol': 4182664683.6247883,
... 'usd_24h_change': 1.2295378479069035, 'last_updated_at': 1549071865}}

"""
