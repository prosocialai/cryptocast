# env.py
from pathlib import Path
import yaml
import os

REPO_DIR = Path(__file__).absolute().resolve().parent.parent.parent
DATA_DIR = REPO_DIR / 'data'
if not DATA_DIR.is_dir():
    os.mkdir(DATA_DIR)
globals().update(yaml.full_load(open(REPO_DIR / '.env').read().replace('=', ': ')))
